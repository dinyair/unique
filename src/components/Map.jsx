import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import GoogleMapReact from 'google-map-react';



const styles = theme => ({ 

});

 class Map extends Component {
  constructor() {
    super();
    this.state = {
    }
  }
  

  
  render() {
    const {classes} = this.props
    const AnyReactComponent = ({ text }) => <div>{text}</div>;

    return (
        <div style={{ height: 250, width: '94%', margin: '21.5% auto auto auto' , border: '3px solid lightblue' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyB8CMCTaZNTG2AZFQyRMJCykf0k9Lqm_9Q' }}
          defaultCenter={{
            lat: 59.95,
            lng: 30.33
          }}
          defaultZoom={11}
        >
          <AnyReactComponent
            lat={59.955413}
            lng={30.337844}
            text="My Marker"
          />
        </GoogleMapReact>
      </div>
    );
  }
}

export default withStyles(styles)(Map)