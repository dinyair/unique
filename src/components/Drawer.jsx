import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';

const useStyles = makeStyles({
    root: {
        width: '97vw',
        height: '97vh',
        bottom: '1.5vh',
        margin: 'auto',
        borderRadius: '2vh',
        "overflow-scrolling": 'touch',
      },
      
  });

export default function Drawer(props)  {
    const classes = useStyles();
    return (
      <div>
            <SwipeableDrawer
                anchor={props.anchor}
                open={props.bool}
                classes={{ paper: classes.root}}
                onClose={props.showHide.bind(this,false)}
                onOpen={props.showHide.bind(this,true)}
                disableSwipeToOpen={true}
                // disableBackdropTransition={!iOS} 
                // disableDiscovery={iOS}
            >
             {props.component}
            </SwipeableDrawer>
        ))}
      </div>
    );
  }
