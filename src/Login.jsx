/* global FB */
import React, { Component } from 'react';
import config from './config.json';
import axios from 'axios';
import { withStyles } from '@material-ui/core/styles';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin  from 'react-google-login';
import InstagramLogin from 'react-instagram-login';
import {ReactSVG} from 'react-svg'
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Grid from '@material-ui/core/Grid';
import { Button } from '@material-ui/core';

const styles = theme => ({
 container:{
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  width: '100%',
  height: '100vh',
 },

 form:{
  display: 'flex',
  flexDirection: 'column',
  alignItems:'center',
  justifyContent: 'center',
  widht: '100%',
  margin: 'auto',
  
  
 },

 margin:{
  margin:theme.spacing(1),
  color: 'black!important',
   width: '70vw',
  '& .MuiInput-underline:after': {
    borderBottomColor: '#96E2EC', // Solid underline on focus
  },
},

 textField: {
  fontFamily: 'Montserrat',
  fontSize: 14,
  lineHeight: 17,
  color: '#5D535E',
  
 },
 labelRoot: {
  fontFamily: 'Montserrat',
  color: '#5D535E',
  fontSize: 14,
  "&$labelFocused": {
    color: '#5D535E',
    fontSize: 14,
  }
},
labelFocused: {},

 button_green: {
  width: '35vw',
  height: '7vh',
  background: '#DFE166',
  fontSize: 15,
  color: 'white',
  boxShadow: '2px 4px 10px rgba(0, 0, 0, 0.05)',
  borderRadius: 50,
  textTransform: 'capitalize',
  margin: '5vh 0 3vh 0 ',
  '&$focused, &:hover': {
    background: '#DFE166',
  },
},

text_description: {
  fontFamily: 'Montserrat',
  fontSize: 15,
  lineHeight: 0,
  color: '#C4C4C4',
},

button_blue: {
  width: '35vw',
  height: '7vh',
  background: 'transparent',
  fontWeight: '500',
  fontSize: 15,
  color: '#96E2EC',
  boxShadow: '2px 4px 10px rgba(0, 0, 0, 0.05)',
  borderRadius: 50,
  textTransform: 'capitalize',
  margin: '3vh 0 0 0 ',
  '&$focused, &:hover': {
    background: 'transparent',
  },
},


 Social:{
  display: 'flex',
  flexDirection: 'row',
  width: '100%',
  margin: 'auto auto 0 auto',
 },



 socialItem:{
 
  display: 'flex!important',
  flexDirection: 'column!important',
  boxShadow: 'unset!important',
  alignItems: 'center!important',
  justifyContent:'center!important',
  fontSize: 16,
  width:'33vw',
  height: '14vh',
  padding: '0!important',
  opacity:'1!important',

  fontStyle: 'normal',
  fontWeight: 'normal',
  border: 'unset!important',
  backgroundColor: 'transparent!important',
  fontFamily: 'Montserrat!important',
  "&>span":{
  fontSize: 16,
  color: 'black!important',
  }
 }
  });




class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      view: 0,
      email : '',
      password: ''
    };
  }

  handleInputChange = (event) => {
    const { value, name } = event.target;
    this.setState({
      [name]: value
    });
  }

  facebookResponse = (response) => {
    console.log('facebookResponse:',response)
    const { email,name, userID } = response
    this.onSubmit('login',email,name, ` x1-${userID}`)
  }

  googleResponse = (response) => {
    console.log('googleResponse:',response)
    const { email,name, userID } = response
    this.onSubmit('login',email,name, ` x1-${userID}`)
  }

  
  

  onSubmit = (type, email, name, password, e) => {
    if( e ) {e.preventDefault();}
    axios.post(config.IpPort+'/'+type,{email, name, password})
    .then(res => {
      if (res.status === 200 ) {
       this.props.setUser(res.data)
      }
        else {
        const error = new Error(res.error);
        throw error;
      }
    
    })
    .catch(err => {
    //  if (err.response.status == 401) {
    //    this.onSubmit('register',email, name, password)
    //  }
      console.error(err);
    });
  }

  render() {
    const {classes} = this.props
    const {email, name, password, view} = this.state
    
    return (
     <div className={classes.container}>

          <ReactSVG style={{lineHeight:0, padding: '5vh 0'}} src={'assets/logo.svg'} />



          <div className={classes.Social}>

          


            <InstagramLogin
            clientId="5fd2f11482844c5eba963747a5f34556"
            cssClass={classes.socialItem}
            onSuccess={this.responseInstagram}
            onFailure={this.responseInstagram}
            >
            <ReactSVG style={{lineHeight:0, paddingBottom: '2vh'}} src={'assets/instagram_icon.svg'} />
            <span>Instagram</span>
            </InstagramLogin>


           

            <GoogleLogin
            clientId="658977310896-knrl3gka66fldh83dao2rhgbblmd4un9.apps.googleusercontent.com"
            className={classes.socialItem}
            // onSuccess={responseGoogle}
            // onFailure={responseGoogle}
            icon={null}
            autoLoad={false}
            cookiePolicy={'single_host_origin'}
            >
              <ReactSVG style={{lineHeight:0, paddingBottom: '2vh'}} src={'assets/google_icon.svg'} />
              <span>Google</span>
              </GoogleLogin>


              <FacebookLogin
                                textButton={'Facebook'}
                                cssClass={classes.socialItem}
                                appId={config.FACEBOOK_APP_ID}
                                fields="name,email,picture"
                                callback={this.facebookResponse}
                                // onFailure={this.onFailureFB}
                                autoLoad={false}
                                icon={ <ReactSVG style={{lineHeight:0, paddingBottom: '2vh'}} src={'assets/facebook_icon.svg'} />}
           />

           
            </div>   


     {view == 0 && (   

<form className={classes.form}
onSubmit={(e)=>this.onSubmit('login',email, name, password, e)}>
<Grid container spacing={1} alignItems="center" style={{justifyContent: 'center',alignItems:'center'}}>
<Grid item>
<ReactSVG style={{lineHeight:0}} src={'assets/email_icon.svg'} />
</Grid>
<Grid item>
<TextField
    className={classes.margin}
    label="E-Mail"
    name="email"
    value={this.state.email}
    onChange={this.handleInputChange}
    required
    InputProps={{
      className: classes.textField,
    }}
    InputLabelProps={{
      classes: {
        root: classes.labelRoot,
        focused: classes.labelFocused
    }
    }}
  />
</Grid>
</Grid>


<Grid container spacing={1} alignItems="center">
<Grid item>
<ReactSVG style={{lineHeight:0}} src={'assets/lock_icon.svg'} />
</Grid>
<Grid item>
<TextField
    className={classes.margin}
    type="password"
    label="Password"
    name="password"
    value={this.state.password}
    onChange={this.handleInputChange}
    required
    InputProps={{
      className: classes.textField,
    }}
    InputLabelProps={{
      classes: {
        root: classes.labelRoot,
        focused: classes.labelFocused
    }
    }}
  />
</Grid>
</Grid>
        
     
        <Button className={classes.button_green} type="submit">Signin</Button>

        <span className={classes.text_description}>or</span>

        <Button className={classes.button_blue} onClick={()=> this.setState({view: 1})}>Signup</Button>
      </form>
     )}


    {view == 1 && (

<form className={classes.form}
onSubmit={(e)=>this.onSubmit('register',email, name, password, e)}>
<Grid container spacing={1} alignItems="center" style={{justifyContent: 'center',alignItems:'center'}}>
<Grid item>
<ReactSVG style={{lineHeight:0}} src={'assets/email_icon.svg'} />
</Grid>
<Grid item>
<TextField
    className={classes.margin}
    label="E-Mail"
    name="email"
    value={this.state.email}
    onChange={this.handleInputChange}
    required
    InputProps={{
      className: classes.textField,
    }}
    InputLabelProps={{
      classes: {
        root: classes.labelRoot,
        focused: classes.labelFocused
    }
    }}
  />
</Grid>
</Grid>


<Grid container spacing={1} alignItems="center">
<Grid item>
<ReactSVG style={{lineHeight:0}} src={'assets/lock_icon.svg'} />
</Grid>
<Grid item>
<TextField
    className={classes.margin}
    type="password"
    label="Password"
    name="password"
    value={this.state.password}
    onChange={this.handleInputChange}
    required
    InputProps={{
      className: classes.textField,
    }}
    InputLabelProps={{
      classes: {
        root: classes.labelRoot,
        focused: classes.labelFocused
    }
    }}
  />
</Grid>
</Grid>

<Grid container spacing={1} alignItems="center">
<Grid item>
<ReactSVG style={{lineHeight:0}} src={'assets/lock_icon.svg'} />
</Grid>
<Grid item>
<TextField
    className={classes.margin}
    type="password"
    label="repassword"
    name="repassword"
    value={this.state.repassword}
    onChange={this.handleInputChange}
    required
    InputProps={{
      className: classes.textField,
    }}
    InputLabelProps={{
      classes: {
        root: classes.labelRoot,
        focused: classes.labelFocused
    }
    }}
  />
</Grid>
</Grid>


<Grid container spacing={1} alignItems="center">
<Grid item>
<ReactSVG style={{lineHeight:0}} src={'assets/lock_icon.svg'} />
</Grid>
<Grid item>
<TextField
    className={classes.margin}
    type="text"
    label="name"
    name="name"
    value={this.state.name}
    onChange={this.handleInputChange}
    required
    InputProps={{
      className: classes.textField,
    }}
    InputLabelProps={{
      classes: {
        root: classes.labelRoot,
        focused: classes.labelFocused
    }
    }}
  />
</Grid>

</Grid>

        


<Button className={classes.button_green} type="submit">Signup</Button>

<span className={classes.text_description}>or</span>

<Button className={classes.button_blue} onClick={()=> this.setState({view: 0})}>Signin</Button>
</form>

    )}


  


    </div>                    
    );
  }
}
export default withStyles(styles)(Login)