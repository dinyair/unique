import React, { Component } from 'react';
import { IconButton, Badge, Input,InputAdornment  } from '@material-ui/core';
import { Menu, Mail, Notifications } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
import SwipeableViews from 'react-swipeable-views';
import styled from "styled-components";
//components
import ToolBar from './ToolBar'
import Footer from './Footer'
import MainTabs from './MainTabs'
import NotificationBox from './Nortification/NotificationBox'
import { ReactSVG } from 'react-svg';

//components
import AroundYou from './Tabs/AroundYou'
import Map from './Map'
import Feed from './Feed'
import Profile from './Profile'

const styles = theme => ({ 
  container: {

    height: '86vh',
  },
  header :{
    height: '8vh',
    width: '100%',
  },

   icon:{
    color: 'white',
    fontSize: 25
  },

  header:{
    // position: 'fixed',
    width: '100%',
    top:0,
    zIndex:99,
    left:0,
    // backgroundColor: 'white',
  },
  searchContainer:{
    display: 'flex',
    flexDirection: 'column',
    width: '95%',
   
    border: '2px solid #96E2EC',
    margin: '1vh auto',
    alignItems: 'center',
    transition: 'height 0.3s ease-in',
 
  },

  search:{
    width: '100%',
    fontFamily: 'Montserrat',
    padding: '0 3vw',
    margin: '0.5vh auto',
  },
  

});




 class Home extends Component {
   
  constructor() {
    super();
    this.state = {
      page:0,
      MainTab:0,
      postInView: null,
      showToolBar: false,
      view: 'list',
      showHeader: true,
      show: true,
      scrollPos: 0
    }
    this.handleScroll = this.handleScroll.bind(this);

  }
  

  componentDidMount() {
    // window.addEventListener("scroll", this.handleScroll);
  }

  componentWillUnmount() {
    // window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll(e) {
    const { scrollPos } = this.state;
    this.setState({
      searchContainerOpen: false,
      // postInView: null,
      scrollPos: document.body.getBoundingClientRect().top,
      show: document.body.getBoundingClientRect().top > scrollPos
    });
  }


  handleInputChange = (event) => {
    const { value, name } = event.target;
    this.setState({
      [name]: value,
      searchContainerOpen: true
    });
  }

  changePage = (page) => this.setState({page})
    

componentDidMount(){
 
}


  render() {
    const {page, showToolBar, MainTab, view,postInView, showHeader, search, searchContainerOpen,  showNotificationBox} = this.state
    const {classes} = this.props
    
    return (
      <div className={classes.container}
      
   >
     <Transition>
        <header className={classes.header} 
        className={this.state.show ? "active" : "hidden"}
        style={{
           boxshadow: '0 0.5rem 2rem rgba(0, 0, 0, 0.65)',
          visibility: showHeader ? 'visible' : 'hidden',
          transition: `all 200ms ${showHeader ? 'ease-in' : 'ease-out'}`,
          transform: showHeader ? 'none' : 'translate(0, -100%)' 
        }}
         onClick={search && search.length > 0  ? ()=> this.setState({searchContainerOpen: true}) : null}
         onBlur={()=> this.setState({searchContainerOpen: false})}>
            <div className={classes.searchContainer}
            
             style={searchContainerOpen ?  
              {
              height: '30vh',
              borderRadius: '5%',
             }:{
              height: '4.5vh',
              borderRadius: '25vh',
             }}>
            <Input
            disableUnderline
            autoComplete='off'
             className={classes.search}
             name="search"
             placeholder="Search..."
             value={search}
             onChange={this.handleInputChange}
                startAdornment={
                  <InputAdornment position="start">
                   <IconButton>
                    <ReactSVG style={{lineHeight:0}} src={'assets/search_icon.svg'}/>
                    </IconButton> 
                  </InputAdornment>
                }
                // endAdornment={
                //   <InputAdornment position="end">
                //  {view == 'list' ? 
                //    <IconButton onClick={()=>this.setState({view: 'grid'})}>
                //    <ReactSVG style={{lineHeight:0}} src={'assets/gridview_icon.svg'}/>
                //    </IconButton>
                //    :
                //    <IconButton onClick={()=>this.setState({view: 'list'})}>
                //    <ReactSVG style={{lineHeight:0}} src={'assets/list_icon.svg'}/>
                //    </IconButton>
                // }
                
                //   </InputAdornment>
                // }
              />

              {search && search.length > 0 && (
                  <div style={{width: '95%',margin: '1.5vh auto', fontFamily: 'Montserrat', height: 'auto', overflow: 'auto'}}>
                    <div style={{fontSize: 14, color: 'grey', }}>Do you mean search?</div>
                    <div style={{
                      padding: '0.5vh 0',
                      display: 'flex',
                      flexWrap: 'wrap',
                      fontSize: 14,
                      color: 'lightgrey'
                      
                    }}>
                      {['#feed','#feed','#feed','#feed','#feed'].map(hashtag =>(
                        <div style={{padding: '0 2vw 0 0'}}>{hashtag}</div>
                      ))}
                </div>


                  <div>
                    {['result1', 'result2', 'result3', 'result4', 'result5'].map(result=> (
                      <div style={{fontSize: 23, padding: '0.7vh'}}>{result}</div>
                    ))}
                  </div>
                </div>
                )}
            </div>
        </header>

        </Transition>

        <ToolBar showToolBar={showToolBar}
                 close={()=>this.setState({showToolBar: false})}
                 open={()=>this.setState({showToolBar: true})} />


  <SwipeableViews
       containerStyle={{height: '86vh'}}
       disabled
      //  onScroll={()=>this.setState({searchContainerOpen: false})}
       onScroll={this.handleScroll}
       enableMouseEvents
      //  axis={'y'}
       onChangeIndex={(page) => this.setState({page})}
       index={page}>
         
   
   {page == 0  && (
     <div>
           <MainTabs tab={MainTab}
                  changeTab={(index)=> this.setState({MainTab: index})} />     
    
    
  
    <div style={{padding: '0.5vh 2vw 1vh 2vw'}} >
    
          <Feed 
            postInView={postInView}
            setPostInView={(postInView, _callback)=>this.setState({postInView},_callback())}
          view={this.state.view}
                changePage={this.changePage}/>
      </div>
      </div>
      )}

  
  {page == 1 && (
    <div style={{padding: '0.5vh 2vw 1vh 2vw'}}>
    </div>
    )}

  {page == 2  && (
    <div style={{padding: '0.5vh 2vw 1vh 2vw'}}>
    </div>
    )}


  {page == 3  && (
    <div style={{padding: '0.5vh 2vw 1vh 2vw'}}>
    </div>
    )}

  {page == 4  && (
    <div style={{padding: '0.5vh 2vw 1vh 2vw'}}>
    </div>
    )}



    {page == 5  && (
          <div style={{padding: '0.5vh 2vw 1vh 2vw'}}>
            <Profile  changePage={this.changePage}/>
          </div>
      )}
    </SwipeableViews>


          <Footer page={page}
                  changePage={this.changePage} />
      </div>
    );
  }
}

const Transition = styled.div`
  .active {
    visibility: visible;
    transition: all 200ms ease-in;
  }
  .hidden {
    visibility: hidden;
    transition: all 200ms ease-out;
    transform: translate(0, -100%);
  }
`;

export default withStyles(styles)(Home)