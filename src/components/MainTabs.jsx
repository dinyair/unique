import React, { Component } from 'react';
import {  Tabs, Tab, Divider  } from '@material-ui/core';
// import {} from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';

const CostumTabs = withStyles({
  root: {
    margin: 'auto',
    paddingLeft: '5vw',
  },
  flexContainer: {
    // justifyContent: 'space-evenly',
  },
  indicator: {
    marginLeft: '7vw',
    height: 8,
    width: '8px!important',
    borderRadius: '50%', 
    backgroundColor: '#DFE166',
  },
})(Tabs);

const CostumTab = withStyles(theme => ({
  root: {
    zIndex:2,
    color: 'black',
    textTransform: 'capitalize',
    width: '10vw',
    fontWeight: 'unset',
    fontFamily: "Montserrat",
    borderBottom: '2px solid lightgrey',
    '&:hover': {
    },
    '&$selected': {
      
      color: '#DFE166',
      // borderBottom: 'unset'
    },
    '&:focus': {
    },
  },
  selected: {},
}))(props => <Tab disableRipple {...props} />);


const styles = theme => ({ 
});

 class MainTabs extends Component {
  constructor() {
    super();
    this.state = {
      categories: ['Food', 'Craft', 'Fitness', 'Fitness', 'Wellness', 'Fashion','Food', 'Craft', 'Fitness', 'Fitness', 'Wellness', 'Fashion'],
    }
  }
  




  render() {
    const {categories} = this.state
    const {tab} = this.props
    return(
     <div>
      <CostumTabs value={tab}  scrollButtons={'on'}>
        {categories.map((category, index) => (
            <CostumTab
              label={category}
              onClick={this.props.changeTab.bind(this, index)}
            />
        ))}
      </CostumTabs>       

     </div> 
    )
 }
}
export default withStyles(styles)(MainTabs)

