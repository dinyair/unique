import React, { Component } from 'react';

import Icon from './SvgIcon'

//material 
import { withStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import HomeIcon from '@material-ui/icons/HomeRounded';



const styles = theme => ({
    root: {
       zIndex:10,
        bottom: 0,
        left: 0,
        width: '100%',
        margin: 'auto',
        position: 'fixed',
        backgroundColor: 'white',
    },
    footer: {
        width: '100%',
        backgroundColor: 'transparent',
        height: '100%',
        flexDirection: 'row',
        height: '6vh',
    },
    action:{
      minWidth: 'unset',
      color:'#898989',
      padding:'0!important',
    },     

    SpecialActionContainer:{
        height: '10vh',
        marginTop: '-5vh',
        maxWidth: '23vw',
        minWidth:'23vw',
        backgroundColor: '#f8f8f8',
        borderRadius: '50%',
        padding:'0!important',
      
      },

      SpecialAction:{
        boxShadow: '0px 17px 10px -10px rgba(0,0,0,0.4)',
        padding: '2vh',
        backgroundColor: 'white',
        borderRadius: '50%',
       },
        

 

    selected:{
      opacity: 1,
      paddingTop:'0!important',
      // borderRadius: '50%',
      // height: 37,
      // width: 37,
      // display: 'flex',
      // justifyContent: 'center',
      // alignItems: 'center',
   
    },
    avatar: {
        backgroundSize: 'contain!important',
        backgroundPosition: 'center!important',
        width: '3.5vh',
        height: '3.5vh',
        borderRadius: '50%',
        opacity: 0.7,
        
        '&$focused, &:hover': {
          // border: '0.2vh solid white',
          borderRadius: '50%',
          padding: '0.5vh',
          opacity: 1,
        },
      },
      icon: { 
        fontSize: '4vh',
        width: '4vh'
      },
      title:{
          color:'grey',
          marginLeft: 20,
          fontSize:'0.9rem',
          textTransform: 'uppercase',
          fontSize: '0.9rem',
          fontWeight: 'bold'
      },
});



class Footer extends Component {
    constructor(props) {
      super(props);
      this.state = {
      }
    }

    render() {
        const {page, classes} = this.props
       
        const list = [
          {  label: 'My Videos', icon: 'assets/home_icon_off.svg', icon_active:'assets/home_icon_on.svg'},
          {  label: 'Favorites', icon: 'assets/map_icon_off.svg', icon_active:'assets/map_icon_on.svg' },
          { label: 'Top', icon: 'assets/map_icon_off.svg', icon_active:'assets/map_icon_on.svg' },
          {  label: 'Feed', icon: 'assets/msg_icon_off.svg', icon_active:'assets/msg_icon_on.svg' },
          { label: 'Profile', icon: 'assets/profile_icon_off.svg', icon_active:'assets/profile_icon.svg' }
          ]
      return ( 
            <footer className={classes.root}>
                   <BottomNavigation
                    value={page}
                    onChange={(event, newValue) => {
                      if( page != newValue ) {
                       if( newValue == 2 ) {

                       }else this.props.changePage(newValue) 
                      }
                      }}
                    showLabels={false}
                    // classes={{ selected: classes.selected }}
                    className={classes.footer}>
                { list.map((item, index) => {
                  if( index != 2 ){
            return (
               <BottomNavigationAction  
                   showLabel={false}
                   disableRipple
                    key={index}
                    value={index}
                    className={ index == 2 ? classes.SpecialActionContainer : page == index ? `${classes.selected} ${classes.action}` : classes.action     } 
                    icon={
                   <div 
                   className={ index == 2 ? classes.SpecialAction : page == index ? `${classes.selected} ${classes.action}` : classes.action   } 
                  > 
                  
                  {page == index ? 
                  <Icon src={item.icon_active }/>:
                  <Icon src={item.icon }/>
                  }
                   
                    </div>  
                  } />          
        )}}) }
      
                    </BottomNavigation>
                   
           </footer>)
    }
}
export default withStyles(styles)(Footer)