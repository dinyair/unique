import React from 'react';
import SVG from 'react-inlinesvg';
 const Icon = (props) =>(
    <SVG
    // baseURL="/"
    cacheRequests={false}
    //   description="The React logo"
    loader={() => <span>Loading...</span>}
    onError={error => console.log(error.message)}
    onLoad={(src, hasCache) => console.log(src, hasCache)}
    preProcessor={props.fill ? code => code.replace(/fill=".*?"/g, `fill="${props.fill}"`) : null}
    src={props.src}
    //   title="React"
    //   uniqueHash="a1f8d1"
    // uniquifyIDs={true}
    />
)
export default Icon;

  