import React, { Component } from 'react';
import { SwipeableDrawer,List, ListItem , ListItemIcon , ListItemText   } from '@material-ui/core';
import { Mail, MoveToInbox } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({ 
    list: {
        width: 250,
      },
      fullList: {
        width: 'auto',
      },
});

 class ToolBar extends Component {
  constructor() {
    super();
    this.state = {
    }
  }
  


   list = () =>{
       const {classes} = this.props
       return (
        <div
        className={classes.list}
        role="presentation"
        onClick={this.props.close}
        onKeyDown={this.props.close}
      >

            <List>
            {['All mail', 'Trash', 'Spam'].map((text, index) => (
            <ListItem button key={text}>
                <ListItemIcon>{index % 2 === 0 ? <MoveToInbox /> : <Mail />}</ListItemIcon>
                <ListItemText primary={text} />
            </ListItem>
            ))}
        </List>

      </div>  
       )
   }
  
  render() {
    const {showToolBar} = this.props
    return (  
        <SwipeableDrawer
        anchor={'left'}
        open={showToolBar}
        onClose={this.props.close}
        onOpen={this.props.open}
        >
        {this.list()}
        </SwipeableDrawer>
    );
  }
}

export default withStyles(styles)(ToolBar)

