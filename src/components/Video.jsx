import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { IconButton } from '@material-ui/core';
import { ReactSVG } from 'react-svg';
import ReactPlayer from 'react-player'

const styles = theme => ({ 
    
    videoContainer: {
            alignItems: 'center',
            justifyContent: 'center',
            display: 'flex',
            width: '100%',
            position: 'relative',
            margin: 'auto auto',
            backgroundColor: 'rgba(0, 0, 0, 0.65)',
    },
    small:{
      "& > video":{
      height: 'calc(100% * 9 / 16)'
      }
    },
  
    ReactPlayer:{
    zIndex:2,
        "& > video":{
         height: 591.44 / 1127.34 * window.innerHeight ,
        width: '100%', 
       objectFit: 'cover'
      }

    },
});

 class Video extends Component {
  constructor() {
    super();
    this.state = {
      isPlaying: false,
      paused: false,
    }
  }
  
  static getDerivedStateFromProps(nextProps, nextState)
  {
    if( nextProps ) {
      if( nextProps.postInView == nextProps.index ) {
        return{ isPlaying: true }
      } else {
        return { isPlaying: false}
      }
    }
  }
  

  
  render() {
    const {isPlaying,paused} = this.state
    console.log(paused)
    const {classes, video, index, postInView, view, small} = this.props
    return (  
        <div className={classes.videoContainer}>
         {paused && (
          <div style={{
            height:  small ? 'calc(100% * 9 / 16)' : 591.44 / 1127.34 * window.innerHeight,
            position: 'absolute',
            top: small ? '36%' :'42%',
            zIndex:3
          }}> 
           <IconButton
           disableRipple
           onClick={()=>this.setState({paused: false})}
           style={{ color: 'lightgrey', fontSize: view == 'list' ? '8em' : '3em'}}>
            <ReactSVG style={{lineHeight:0}} src={'assets/play_icon.svg'}/>
           </IconButton>
          </div> 
            )}
       

      <ReactPlayer
        playsInline={true}
        playing={isPlaying && !paused}
        // controls={true}
        url={video}
        onClick={()=>{this.setState({paused: !paused})}}
        className={small ? `${classes.ReactPlayer} ${classes.small}` : `${classes.ReactPlayer}`}
  
        ref={'video'}
        />
        </div>

    );
  }
}



export default withStyles(styles)(Video)



