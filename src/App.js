import React, { Component } from 'react';
import config from './config';

//components
import withAuth from './withAuth';
import Home from './components/Home';
import Secret from './Secret';
import Login from './Login';
import { withCookies, Cookies } from 'react-cookie';
const cookies = new Cookies();

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      isAuth: false,
    }
  }


  componentDidMount() {
  

    window.oncontextmenu = function(event) { // fix long press menu bug
      event.preventDefault();
      event.stopPropagation();
      return false;
    };

    
    if( cookies.get('userID') && cookies.get('token') ) {
      this.setState({isAuth: true})
    }
  }



  render() {
    const {isAuth}  = this.state

    if( isAuth ) { 
    return  <Home 
    logOut = {()=>{
      cookies.remove("userID");
      cookies.remove("token");
      this.setState({isAuth: false})

    }}/> }
    else { 
      return <Login 
      setUser={(userObj)=>{
      this.setState({isAuth: true})   
      //set cookies
        let d = new Date();
        d.setTime(d.getTime() + (24*60*60*1000));
        cookies.set("userID", userObj.userID, {path: "/", expires: d});
        cookies.set("token", userObj.token, {path: "/", expires: d});
      }} /> }
  }
}

export default withCookies(App);
